import java.util.Scanner;

public class PCHaendler {
	
	public static String Artikel(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String art = myScanner.next();
		return art;
	}
		
		
	public static int Anzahl(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int anz = myScanner.nextInt();
		return anz;
	}
	
	
	
	public static double Preis(String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int preis = myScanner.nextInt();
		return preis;										
	
	}										
	
	public static double Mehrwertsteuer (String text) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int mwst = myScanner.nextInt();
		return mwst;
												
	}											
										
	
	
	
	public static void main(String[] args) { 
		Scanner myScanner = new Scanner(System.in);
		
		 String artikel = Artikel ("was m�chten sie bestellen?");
		 
		 int anzahl = Anzahl ("Geben Sie die Anzahl ein");
		 
		 double preis = Preis ("Geben Sie den Nettopreis ein:");
		 
		 double mwst = Mehrwertsteuer ("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		 
		


		// Verarbeiten
		double nettogesamtpreis = anzahl * preis;
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);

		// Ausgeben

		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}


	
	
}
