package anfaenger;

public class LadungTest {

	public static void main(String[] args) {
		
		// Konstruktor
		Ladung l1 = new Ladung();
		
		l1.setBezeichnung("Photonentorpedo");
		l1.setMenge(12);
		
		System.out.println("Name: " + l1.getBezeichnung());
		System.out.println("Gehalt: " + l1.getMenge());
		System.out.println();
		
		//vollparametisierter Konstruktor
		Ladung l2 = new Ladung("Wasser", 2800);
		
		System.out.println("Name " + l2.getBezeichnung());
		System.out.println("Gehalt " + l2.getMenge());
		System.out.println();
		
		
		
		
	}

}
