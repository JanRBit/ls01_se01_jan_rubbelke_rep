package anfaenger;

public class Ladung 
{	
	private String bezeichnung; 
	private int menge;
	
	//Konstruktor
	public Ladung() 
	{
	
	}
	//vollparametisierter Konstruktor
	public Ladung(String bezeichnung, int menge)
	{
		this.bezeichnung = bezeichnung; 
		this.menge = menge;
	}
	
	//Setter und Getter
	public void setBezeichnung(String bezeichnung)
	{
		this.bezeichnung = bezeichnung;
	}
	
	public String getBezeichnung()
	{
		return this.bezeichnung;
	}
	
	public void setMenge(int menge)
	{
		this.menge = menge;
	}
	
	public int getMenge()
	{
		return this.menge;
	}
	
}	
