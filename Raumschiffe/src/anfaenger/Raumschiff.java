package anfaenger;

import java.util.ArrayList;

public class Raumschiff
{
	//Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int	schildeInProzent;
	private int	huelleInProzent;
	private int	lebenserhaltungssystemeInProzent;
	private int	androidenAnzahl;
	private String schiffsname;
	
	
	ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktor
	public Raumschiff() {
		
	}
	
	//vollparametisierter Konstruktor
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname)
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	//Setter und Getter
	
	//Torpedos
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl)
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public int getPhotonentorpedoAnzahl()
	{
		return this.photonentorpedoAnzahl;
	}
	
	//Energie
	public void setEnergieversorgungInProzent(int energieversorgungInProzent)
	{
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	public int getEnergieversorgungInProzent()
	{
		return this.energieversorgungInProzent;
	}
	
	//Schilde
	public void setSchildeInProzent(int schildeInProzent)
	{
		this.schildeInProzent = schildeInProzent;
	}
	
	public int getSchildeInProzent()
	{
		return this.schildeInProzent;
	}
	
	//Huelle
	public void setHuelleInProzent(int huelleInProzent)
	{
		this.huelleInProzent = huelleInProzent;
	}
	
	public int getHuelleInProzent()
	{
		return this.huelleInProzent;
	}
	
	//Lebenserhaltung
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent)
	{
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	public int getLebenserhaltungssystemeInProzent()
	{
		return this.lebenserhaltungssystemeInProzent;
	}
	//Androiden
	public void setAndroidenAnzahl(int androidenAnzahl)
	{
		this.androidenAnzahl = androidenAnzahl;
	}
	
	public int getAndroidenAnzahl()
	{
		return this.androidenAnzahl;
	}
	
	//Name
	public void schiffsname(String schiffsname)
	{
		this.schiffsname = schiffsname;
	}
	
	public String getSchiffsname()
	{
		return this.schiffsname;
	}
	
	//Ladung hinzufuegen
	public void addLadung(Ladung neueladung) 
	{
		ladungsverzeichnis.add(neueladung);
	}
	
		
	//Ladungsverzeichnis ausgeben
	public void ladungsverzeichnisAusgeben() {
		for(int i = 0; i < ladungsverzeichnis.size(); i++) 
		{
			System.out.print(ladungsverzeichnis.get(i).getBezeichnung() + "    ");
			System.out.print("Menge: " + ladungsverzeichnis.get(i).getMenge());
			System.out.println();
			
		}

	}

	
	public void zustandRaumschiff()	
	{
		System.out.println("Zustand: ");
		System.out.println("Name: " + getSchiffsname());
		System.out.println("Torpedos: " + getPhotonentorpedoAnzahl());
		System.out.println("Energieversorgung: " + getEnergieversorgungInProzent() + "%");
		System.out.println("Schilde: " + getSchildeInProzent() + "%");
		System.out.println("Huelle: " + getHuelleInProzent() + "%");
		System.out.println("Lebenserhaltung: " + getLebenserhaltungssystemeInProzent() + "%");
		System.out.println("Androiden: " + getAndroidenAnzahl());
		System.out.println();
	}
	
	private void treffer()
	{
		System.out.println(getSchiffsname() +" wurde getroffen!");
	}
	
	public void photonentorpedoSchiessen() 
	{
		if (photonentorpedoAnzahl < 1)			
		{
			System.out.println(getSchiffsname() + ": -=*click*=-");
		}
		else
		{
			photonentorpedoAnzahl = photonentorpedoAnzahl -1;
			System.out.println(getSchiffsname() + ": hat einen Photonentorpedo abgeschossen");
			treffer();
		}
	}	
		
	public void phaserkanoneSchiessen()
	{
		if (energieversorgungInProzent < 50)
		{
			System.out.println(getSchiffsname() + ": -=*click*=-");
		}
		else
		{
			energieversorgungInProzent = energieversorgungInProzent - 50;
			System.out.println(getSchiffsname() + ": hat Phaserkanone abgeschossen");
			treffer();
		}
	}
	
	public void nachrichtAnAlle(String message)
		{
			System.out.println(getSchiffsname() + ": " + message);
		}
		
	}
			

			
	
	


