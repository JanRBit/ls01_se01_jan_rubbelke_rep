package anfaenger;

public class Raumschifftest {

	public static void main(String[] args) 
	{
	
		//Klingonen
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Ladung lk1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung lk2 = new Ladung("Bat'leth Klingonen Schwert", 200);
		
		klingonen.addLadung(lk1);
		klingonen.addLadung(lk2);
		
		
		//Romulaner
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara");
		Ladung lr1 = new Ladung("Borg-Schrott", 5);
		Ladung lr2 = new Ladung("Rote Materie", 2);
		Ladung lr3 = new Ladung("Plasma-Waffe", 50);
		
		romulaner.addLadung(lr1);
		romulaner.addLadung(lr2);
		romulaner.addLadung(lr3);
		
		//Vulkanier
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "NI 'Var");
		Ladung lv1 = new Ladung("Forschungssonde", 35);
		Ladung lv2 = new Ladung("Photonentorpedo", 3);
		
		vulkanier.addLadung(lv1);
		vulkanier.addLadung(lv2);
		
		//Ablauf
		klingonen.photonentorpedoSchiessen();
		System.out.println();
		
		romulaner.phaserkanoneSchiessen();;
		System.out.println();
		
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		System.out.println();
		
		klingonen.zustandRaumschiff();
		
		klingonen.photonentorpedoSchiessen();
		klingonen.photonentorpedoSchiessen();
		System.out.println();
		
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		System.out.println();
		
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		System.out.println();
		
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		
		
		
	}

}
