import java.util.Scanner;

class Fahrkartenautomat {

    public static double fahrkartenbestellungErfassen() {
    	Scanner input = new Scanner(System.in);
    	System.out.println("Bitte w�hlen Sie ein Ticket");
		System.out.println("1) Einzelfahrschein Regeltarif AB [2,90 EUR] ");
		System.out.println("2) Tageskarte Regeltarif AB [8,60 EUR] ");
		System.out.println("3) Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]");
		System.out.println("4) Ende");

		System.out.print("\nIhre Auswahl:");
		
		double preis = 0;
		double anzahl = 0;
		int auswahl = input.nextInt();

		switch (auswahl) {
		case 1:
			System.out.printf("Ihre Wahl: %d %n%n", auswahl);
			System.out.println("Wie viele Tickets m�chten Sie?");
			anzahl = input.nextDouble();
			preis = anzahl * 2.90;
			break;
		case 2:
			System.out.printf("Ihre Wahl: %d %n%n", auswahl);
			System.out.println("Wie viele Tickets m�chten Sie?");
			anzahl = input.nextDouble();
			preis = anzahl * 8.60;
			break;
		case 3:
			System.out.printf("Ihre Wahl: %d %n%n", auswahl);
			System.out.println("Wie viele Tickets m�chten Sie?");
			anzahl = input.nextDouble();
			preis = anzahl * 23.50;
			break;
		case 4:
			System.out.printf("Ihre Wahl: %d %n%n", auswahl);
			break;
		
		}
        return preis;	
        		
        		
    }
        
        
        
         
        
    

        
    

    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        double eingeworfenemuenze;
        double eingezahlterGesamtbetrag = 0.0;
        Scanner tastatur = new Scanner(System.in);

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.format("Noch zu zahlen: %4.2f ��� %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
            eingeworfenemuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfenemuenze;
        }
        return eingezahlterGesamtbetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\nIhre Fahrscheine wurden ausgegeben. \n\n");
    }

    public static void rueckgeldAusgeben(double zuZahlen, double gezahlt) {
    	double rueckgabebetrag = gezahlt - zuZahlen;
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der R�ckgabebetrag in H�he von %4.2f ��� %n", rueckgabebetrag, "�");
            System.out.println("wird in folgenden M�nzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-muenzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
       System.out.printf("%n%n");
    }

    public static void main(String[] args) {
    	int n = 1;
    while(n==1) {
    	double gesamt = fahrkartenbestellungErfassen();
    	double gezahlt = fahrkartenBezahlen(gesamt);
    	fahrkartenAusgeben();
    	rueckgeldAusgeben(gesamt, gezahlt);
    }
    	
    	
    	
    }
}